var namespaces_dup =
[
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01", "namespaceHW__0x01.html", [
      [ "getChange", "namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654", null ],
      [ "cng", "namespaceHW__0x01.html#a868cbf54029436fb6e6d4b6aa8146051", null ],
      [ "cngVal", "namespaceHW__0x01.html#a4740893c3a074a5bd64df64e0d401f45", null ],
      [ "denoms", "namespaceHW__0x01.html#a133ac2b808698b3316db8c9211667639", null ],
      [ "pmt", "namespaceHW__0x01.html#a4dbb0a8dbed6fc8408ff541a2b051e8e", null ],
      [ "pmtVal", "namespaceHW__0x01.html#ad638f997e0a837494e540d388d0276ab", null ],
      [ "price", "namespaceHW__0x01.html#a6ffa02ed681f39b50cd02eb097c3f254", null ],
      [ "test_cases", "namespaceHW__0x01.html#a73f90f8633cce5f280ebeb6c31b8abf8", null ]
    ] ],
    [ "main", null, [
      [ "runs", "main_8py.html#ab7be654152e7640ef25fb4d5bd7c0f3f", null ],
      [ "state", "main_8py.html#a930442dd832ba9fe94227723c47f6653", null ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ]
];